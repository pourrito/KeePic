<?php
	// C'EST UN ÉNORME FATRAS OKAY ?

	// config:
	$PASSWORD='VOTRE_MOT_DE_PASSE';
	$SUBDIR='files';
	$scriptname = basename($_SERVER["SCRIPT_NAME"]);
	date_default_timezone_set('Europe/Paris'); // because french baguette omelette du fromage
	$bttn='bouton';


	// disconnect
	if(isset($_GET['deco'])){
		setcookie('actif', '',time()-3600, "/");
		setcookie("expire","a",time()-3600, "/");
		header('Location: index.php');
	}

	// connect
	if(isset($_POST['password']) && $_POST['password'] == $PASSWORD){
		setcookie('actif', $PASSWORD, time()+60*60*2,"/"); // not very secure ?
		setcookie("expire",time()+60*60*2,time()+60*60*2,"/");
	}

	if(isset($_GET["hostImage"])){

		// false password.
		if(!isset($_COOKIE['actif'])){
			if($_POST['password'] != $PASSWORD){
				print 'Mauvais mot de passe.';
				exit();
			}
		}

		$img = $_POST["image"];
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file_name = time(). '.png';
		$file_path = realpath("./". $SUBDIR ."/");
		$file_path = $file_path . "/" . $file_name ;

		if(file_put_contents($file_path , $data)) {
			 echo $SUBDIR ."/". $file_name;
		} else{
			echo "error!";
		}
		exit();
	}

?>
<!DOCTYPE html>
<html>
<head>
<title>KeePic</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="icon" type="image/png" href="img/favicon.png" />
<link rel="stylesheet" href="design.css" />
</head>
<body><div class="contenu">
<?php if(isset($_COOKIE['actif'])){ echo " <div class='deconx'> <a href=\"?deco\" title='Déconnexion'>&nbsp;</a></div>"; } ?> 
	<h1>KeePic</h1>

	<?php 
			// A simple, minimalist, personal file/image hosting script. - version 0.6
			// Only you can upload a file or image, using the password ($PASSWORD).
			// Anyone can see the images or download the files.
			// Files are stored in a subdirectory (see $SUBDIR).
			// This script is public domain.
			// Source: http://sebsauvage.net/wiki/doku.php?id=php:imagehosting
			// Added by Corentin Bettiol : list of files in the $SUBDIR subdirectory, 2h cookie connexion memory, including timestamp in the filenames, hosting images in clipboard by pressing ctrl+v.


	if(isset($_FILES['filetoupload']))
	{
		// sleep(3); // Reduce brute-force attack effectiveness. You can activate it if you have experienced attacks on your server.

		// false password.
		if(!isset($_COOKIE['actif'])){
			if($_POST['password'] != $PASSWORD){
				print 'Mauvais mot de passe.';
				exit();
			}
		}

		// timestamp in name file
		$filename = $SUBDIR.'/'. time() .'-'. basename($_FILES['filetoupload']['name']);

		// timestamp should prevent this
		if(file_exists($filename)){
			print 'Ce fichier existe déjà.'; exit();
		}

		// hosting the file and displaying the url
		if(move_uploaded_file($_FILES['filetoupload']['tmp_name'], $filename))
		{
			$serverport='';
			
			if ($_SERVER["SERVER_PORT"]!='80') {
				$serverport=':'.$_SERVER["SERVER_PORT"];
			}
			
			$fileurl='http://'.$_SERVER["SERVER_NAME"].$serverport.dirname($_SERVER["SCRIPT_NAME"]).'/'.$filename;
			
			echo 'Le fichier est accessible à cette addresse<br /><br /> <a class="link" href="'.$fileurl.'">'.$fileurl.'</a>';
		}

		// error during the hosting process
		else{
			echo "Il y a eu une erreur d'hébergement du fichier, réessayez svp.";
		}
		echo '<br><br><div class="retour"><a href="'.$scriptname.'">Héberger un autre fichier</a> <a href="'. $SUBDIR .'/index.php">Voir les fichiers hébergés</a></div>';
		
		exit();
	}
	?>

	<form method="post" action="index.php" enctype="multipart/form-data">        
		<input id="file" type="file" name="filetoupload" class="select"><label for="file"><svg xmlns="http://www.w3.org/2000/svg"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg><span> Sélection</span></label>
		<input type="hidden" name="MAX_FILE_SIZE" value="512000000"><br>
    <input type="password" name="password" class="mdp" placeholder="Mot de passe"><br>
    <input type="submit" value="Envoyer" class="envoi">   
	</form>

	<p id="hostedImage">

	</p>

	<p>
		<a class="link" href="<?php echo $SUBDIR; ?>/index.php">Voir les fichiers hébergés</a>
	</p>
</div>
<div class="credits"><small>Script original par <a href="http://sebsauvage.net/wiki/doku.php?id=php:filehosting">sebsauvage.net</a> et <a href="http://l3m.in/">l3m.in</a> - 
<a href="https://gitlab.com/pourrito/KeePic" target="_blank">Source</a></small></small></div>
	<?php if(isset($_COOKIE['actif']) && $_COOKIE['actif'] ==$PASSWORD) ?>
<script src="dist/custom-file-input.js"></script>
</body>
</html>
